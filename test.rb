module Base
  class Animal
    def can_walk?
      return true
    end
  end
end

module Custom
  class Animal
    def can_fly?
      return true
    end
  end
end

module DateHelper
  def get_current_day
    Date.today.to_s
  end
end

module MathHelper

  def circle_area(r)
    pi * r**2
  end

  def pi
    3.14
  end
end

class Human < Base::Animal
  attr_accessor :name
  attr_writer :age
  def initialize name, age, gender
    @name = name
    @age = age
    @gender = gender
  end

  def get_age
    @age
  end

  include DateHelper
  extend MathHelper

  protected
  def get_gender
    @gender
  end

  private
  def get_pin_code
    rand(1000..9999)
  end
end

class Bird < Custom::Animal
  attr_accessor :name
  attr_writer :age
  def initialize name, age
    @name = name
    @age = age
  end

  def get_age
    @age
  end
end

max = Human.new("Max", 26, 'male')
# puts max.get_age
# puts max.name
# max.age = 27
# puts max.get_age
# puts max.can_walk?
# puts max.get_current_day
# puts Human.circle_area(5)

# puts max.get_gender # should be an error "protected method"
# puts max.get_pin_code # should be an error "protected method"

# ********* Можете ли вы назвать мне три уровня методов контроля доступа для классов и модулей? Что они подразумевают? **************

# class AccessLevel
#   def something_interesting
#     another = AccessLevel.new
#     another.public_method
#     another.protected_method
#     another.private_method
#   end
#   def public_method
#     puts "Public method. Nice to meet you."
#   end
#   protected
#   def protected_method
#     puts "Protected method. Sweet!"
#   end
#   private
#   def private_method
#     puts "Incoming exception!"
#   end
# end
#
# AccessLevel.new.something_interesting

# ************** Есть три способа вызвать метод в ruby. Можете ли вы назвать мне хотя бы два? ***************

# max = Human.new("Max", 25, 'male')
# puts max.object_id
# puts max.send(:object_id)
# puts max.method(:object_id).call


# ***************** Что означает self? *******************

class WhatIsSelf
  def test
    puts "At the instance level, self is #{self}"
  end
  def self.test
    puts "At the class level, self is #{self}"
  end
end
WhatIsSelf.test
#=> At the class level, self is WhatIsSelf
WhatIsSelf.new.test