"use strict";

$(function(){
    ContentLengthCounter.init();
});

var ContentLengthCounter = {
    init: function(){
        this.bindEvents();
    },
    bindEvents: function() {
        var handler = this;
        var $form_input = $('#new_micropost textarea');
        var $content_length = $('#content-length');
        $form_input.on('keydown', function(e) {
            var content = $form_input.val();
            if ((e.keyCode != 8) && (e.keyCode != 46 )) {
                $content_length.text(content.length + 1);
            } else {
                if ((content.length - 1) < 1) {
                    $content_length.text(0);
                } else {
                    $content_length.text(content.length - 1);
                }
            }
            if ((content.length + 1) >= 140) {
                $form_input.val(content.substring(0, 140));
                handler.markImportant($content_length)
            }
            if (((content.length + 1) >= 100) && ((content.length + 1) < 140)) {
                handler.markWarning($content_length);
            }
            if (((content.length + 1) >= 1) && ((content.length + 1) < 100)) {
                handler.markSuccess($content_length);
            }
            if ((content.length + 1) < 1) {
                $content_length.removeClass('label-success');
                $content_length.text(0);
            }

        });
        $form_input.on('keyup', function() {
            var content = $form_input.val();
            $content_length.text(content.length);
            if ((content.length) >= 140) {
                $form_input.val(content.substring(0, 140));
                $content_length.text(140);
                handler.markImportant($content_length)
            }
            if (((content.length) >= 100) && ((content.length) < 140)) {
                handler.markWarning($content_length);
            }
            if ((content.length) < 100) {
                handler.markSuccess($content_length);
            }
            if ((content.length) == 0) {
                $content_length.removeClass('label-success');
            }
        });
    },
    markSuccess: function(container) {
        this.removeClasses(container)
        container.addClass('label-success');
    },
    markWarning: function(container) {
        this.removeClasses(container)
        container.addClass('label-warning');
    },
    markImportant: function(container) {
        this.removeClasses(container)
        container.addClass('label-important');
    },
    removeClasses: function(container) {
        container.removeClass('label-success label-warning label-important');
    }
};
