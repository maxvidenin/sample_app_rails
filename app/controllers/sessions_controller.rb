class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      sign_in user
      flash[:success] = 'You successfully authorized!'
      redirect_back_or user
    else
      flash.now[:error] = 'Email or password invalid!'
      render :new
    end
  end

  def destroy
    sign_out
    redirect_to root_url
  end

  private

  # def sessions_params
  #   params.require(:session).permit(:email, :password)
  # end
end
